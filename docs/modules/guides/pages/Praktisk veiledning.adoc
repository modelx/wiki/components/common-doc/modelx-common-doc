= Praktisk veiledning
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: master@common-doc:guides:
endif::[]
:toc: left
:toclevels: 4
:sectnums:
:sectnumlevels: 9

TBD

:leveloffset: +1
include::master@common-doc:guides:page$Hvordan komme i gang med aktuelle verktøy.adoc[]
include::master@common-doc:guides:page$Hvordan dele et dokument .adoc[]
include::master@common-doc:guides:page$Hvordan opprette et ad-hoc samarbeid.adoc[]
:leveloffset: -1
