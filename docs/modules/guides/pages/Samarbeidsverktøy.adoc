= Samarbeidsverktøy
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: master@common-doc:guides:
endif::[]
:toc: left
:toclevels: 4
:sectnums:
:sectnumlevels: 9

Foreløpig liste:

* gitlab
* Slack
* MS Teams 
* Google Docs


