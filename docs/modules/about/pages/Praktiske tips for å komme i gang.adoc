= Praktiske tips for å komme i gang
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: master@common-doc:about:
endif::[]
:toc: left
:toclevels: 4
:sectnums:
:sectnumlevels: 9



:leveloffset: +1
include::master@common-doc:about:page$Hvordan opprette et ad-hoc samarbeid i gitlab.adoc[]
:leveloffset: -1
