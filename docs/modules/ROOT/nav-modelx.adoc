:lang: no
// include::architecture-repository:common:partial$commonincludes.adoc[]

// :lang: no ------------>
ifeval::["{lang}" == "no"]

* xref:common-doc:about:Om ModelX.adoc[Om ModelX]
* xref:common-doc:initiatives:Nettverksaktiviteter.adoc[Nettverksaktiviteter]
* xref:common-doc:showrooms:Utstillingsvinduer.adoc[Utstillingsvinduer]
* xref:common-doc:for-reuse:Gjenbruksstasjon.adoc[Gjenbruksstasjon]
* xref:common-doc:guides:Veiledere.adoc[Veiledere]


//* xref:index.adoc[Om UNIT referansearkitekturer]
// * xref:common-doc:ROOT:index.adoc[Om ModelX]

endif::[]
// :lang: no <-----------
 


// :lang: en ------------>
ifeval::["{lang}" == "en"]

* xref:index.adoc[About ModelX]

** xref:index.adoc[Welcome]

endif::[]
// :lang: en <-----------

