= Model X
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: master@modelx:ROOT:
endif::[]
:toc: left
:toclevels: 3
:sectnums:
:sectnumlevels: 9


//:leveloffset: +1
include::master@common-doc:books:page$ModelX - Fra modellutveksling til arkitektursamarbeid.adoc[]

//:leveloffset: -1
